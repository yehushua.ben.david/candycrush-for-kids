import pygame as pg
import random as rnd
import time as tps
import sys

pg.init()
pg.mixer.init()
s_swap = pg.mixer.Sound("snd/swap.ogg")
s_bang = pg.mixer.Sound("snd/bang.ogg")
pg.mixer.music.load("snd/intro.mp3")
pg.mixer.music.play(-1)
sz=64
def create2D(w,h,val=lambda x,y:0):
    if callable(val):
        getval=val
    else:
        getval = lambda x,y:val
    return [[getval(x,y) for x in range(w) ] for y in range(h)]
b=create2D(6,8,0)
sprites={}
sprites[1]=pg.image.load("img/1.png")
sprites[2]=pg.image.load("img/2.png")
sprites[3]=pg.image.load("img/3.png")
sprites[4]=pg.image.load("img/4.png")
sprites[5]=pg.image.load("img/5.png")
sprites[6]=pg.image.load("img/6.png")

disp=pg.display.set_mode((sz*6,sz*8))
pg.display.set_caption("Candy Crush 4 Kids")
def removeOneHole():
    for x in range(6):
        for y in range(7,-1,-1):
            if (b[y][x]==0):
                for yy in range(y,0,-1):
                    b[yy][x]=b[yy-1][x]
                    b[yy - 1][x] = 0
                b[0][x]= rnd.randint(1,6)
                s_swap.play()
                return 1
    return 0
def removeThree():
    listzero = []
    for x in range(6):
        for y in range(8):
            if 0<y<7:
                if (b[y - 1][x] == b[y][x]) and (b[y + 1][x] == b[y][x]):
                    listzero += [(x, y), (x, y - 1), (x, y + 1)]
            if 0<x<5:
                if (b[y][x+1] == b[y][x]) and (b[y ][x-1] == b[y][x]):
                    listzero += [(x, y), (x+1, y ), (x-1, y)]

    for p in listzero:
        b[p[1]][p[0]]=0
        s_bang.play()
        drawfx()
        pg.display.update()
        tps.sleep(0.2)
    return len(listzero)

def drawfx():
    disp.fill([255]*3)
    for x in range(6):
        for y in range(8):
            if b[y][x] in sprites:
                disp.blit(sprites[b[y][x]],(x*64,y*64))

selected = 0
while 1:
    if removeOneHole():
        tps.sleep(0.05)
    else:
        removeThree()
    drawfx()
    pg.display.update()
    for e in pg.event.get():
        if e.type == pg.MOUSEBUTTONDOWN:
            selected = (e.pos[0]//sz,e.pos[1]//sz)
        if e.type==pg.MOUSEBUTTONUP:
            pos = (e.pos[0]//sz,e.pos[1]//sz)
            if (abs(selected[0]-pos[0]) + abs(selected[1]-pos[1]) ) == 1:
                b[pos[1]][pos[0]],b[selected[1]][selected[0]]  = b[selected[1]][selected[0]],b[pos[1]][pos[0]]
                drawfx()
                pg.display.update()
                tps.sleep(0.7)
                if not removeThree():
                    b[pos[1]][pos[0]], b[selected[1]][selected[0]] = b[selected[1]][selected[0]], b[pos[1]][pos[0]]

            selected=0
            s_swap.play()
        if e.type==pg.QUIT:
            sys.exit()
